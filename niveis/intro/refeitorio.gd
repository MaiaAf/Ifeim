extends Node

@export var dialogo: DialogueResource

const NIVEL_UM : String = "res://niveis/n1/nivel_1.tscn"
const NIVEL_DOIS : String = "res://niveis/n1/salas/sala_2.tscn"

@onready var inicio_do_jogo: Node = $"."
@onready var bru: Npc = $"../turma/Bru"
@onready var kay: Npc = $"../turma/Kay"
@onready var bia: Npc = $"../turma/Bia"


@onready var npc: Npc = $Helb
@onready var marker_eletro: Marker2D = $MarkerEletro
@onready var cut: Cutscene = $"../Cutscene"
@onready var npc_cutscene: Cutscene = $Helb/NpcCutscene
@onready var refeitorio_interagir: Interagir = $Marker2D3/refeitorioInteragir


func _ready() -> void:
	refeitorio_interagir.desativar = true
	if GerenciadorDeMissoes.is_missao_completa("n1_sl1"):
		if GerenciadorDeMissoes.is_missao_completa("n1_sl2"): return
		bia.global_position = $Marker2D.global_position
		bru.global_position = $Marker2D2.global_position
		kay.global_position = $Marker2D3.global_position
		$"../turma".show()
		# Introdução para o nível de eletromecânica
		await get_tree().physics_frame
		Global.Dialogo(dialogo, "fim_sala1", false, kay)
		await DialogueManager.dialogue_ended
		
		cut.tween_geral([cut.jogador, npc],[0.0, 1.0])
		await cut.tween.finished
		npc_cutscene.ativar_camera(true, false, false)
		npc.seguir(marker_eletro.global_position)
		await get_tree().create_timer(6).timeout
		cut.tween_geral([npc, cut.jogador],[0.0,2.0], true)
		await cut.tween.finished


func _on_interagir_interagir() -> void:
	Global.Dialogo(dialogo, "lab4")
	await DialogueManager.dialogue_ended
	if GerenciadorDeMissoes.variaveis_dialogo["seguir_refeitorio"]:
		refeitorio_interagir.desativar = false
		bia.seguir($Marker2D.global_position)
		bru.seguir($Marker2D2.global_position)
		kay.seguir($Marker2D3.global_position)
		await bia.alvo_alcancado
		

func _on_refeitorio_interagir() -> void:
	if not GerenciadorDeMissoes.is_missao_completa("n1_sl1"):
		Global.Dialogo(dialogo, "nivel1")
		await DialogueManager.dialogue_ended
		Global.carregar_cena(NIVEL_UM)


func _on_area_cair_itens_body_entered(body: Node2D) -> void:
	if body is Npc and body.nome == "Helb":
			body.criar_objeto("res://cenas/objetos/Multimetro.tscn")
			await get_tree().create_timer(6).timeout
			Global.Dialogo(dialogo, "inicio_sala2")


func _on_eletro_item_body_entered(body: Node2D) -> void:
	if body.is_in_group("Multimetro"):
		Global.Dialogo(dialogo, "Helb_1", false, kay)
		# Carregar próxima cena de forma assíncrona
		Global.carregar_cena(NIVEL_DOIS, false)
		# Virar npc para o personagem
		npc.animacao.flip((npc.global_position.x 
				- cut.jogador.global_position.x) > 0)
		bru.seguir($MarkerEletro/Turma1.global_position)
		bia.seguir($MarkerEletro/Turma2.global_position)
		kay.seguir($MarkerEletro/Turma3.global_position)
		await DialogueManager.dialogue_ended
		Global.carregar_cena(NIVEL_DOIS)
