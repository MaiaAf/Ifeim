extends Node
@export var diálogo1 : DialogueResource

@onready var cutscene: Cutscene = $Cutscene

@onready var pos_1: Marker2D = $marcadores/pos1
@onready var pos_2: Marker2D = $marcadores/pos2
@onready var pos_3: Marker2D = $marcadores/pos3

@onready var animation_player: AnimationPlayer = $AnimationPlayer
@onready var bru: Npc = $"../npcs_tut/Bru"
@onready var bru_cut: Cutscene = $"../npcs_tut/Bru/BruCut"



@onready var alvo_npc: Marker2D = $marcadores/alvo_npc
@onready var alvo_npc_2: Marker2D = $marcadores/alvo_npc2

var jogador : Personagem

func _ready() -> void:
	jogador = get_tree().get_first_node_in_group("Player")
	passeio()


func passeio() -> void:
	Global.pode_mover = false
	cutscene.zoom = Vector2(0.7, 0.7)
	cutscene.tween_geral([pos_1, pos_2], [6.0, 4.0,], false)
	get_tree().create_timer(1.0).timeout.connect(
	func () -> void: cutscene.tween_zoom(Vector2(0.2, 0.2),4.0)
	)

	await cutscene.tween.finished
	cutscene.tween_zoom(Vector2(1.0, 1.0), 4.0)
	cutscene.tween_geral([jogador], [4.0], false)
	await cutscene.tween.finished
	
	await get_tree().create_timer(1.0).timeout
	Global.Dialogo.call_deferred(diálogo1, "boas_vindas")


func _on_pegar_item_body_entered(body: Node2D) -> void:
	if body is Npc and body.nome == "Bru":
		body.criar_objeto("res://cenas/objetos/Computador.tscn")
	if body is Personagem:
		Global.pode_mover = false
		bru.seguir(alvo_npc.position)
		cutscene.tween_geral([jogador, bru],[0.0, 1.0], true)
		await cutscene.tween.finished
		
		bru_cut.ativar_camera(true, false, false)
		GerenciadorDeMissoes.atualizar_missao("tut_1", 1)



func _on_sujeito_item_body_entered(body: Node2D) -> void:
	if body.is_in_group("Computador"):
		# Se virar para o personagem
		for npc : Npc in [bru, $"../npcs_tut/Kay", $"../npcs_tut/Bia"]:
			npc.animacao.flip((npc.global_position.x 
					- jogador.global_position.x) > 0) 
		Global.Dialogo.call_deferred(diálogo1, "coisa_entregue")
		await DialogueManager.dialogue_ended
		body.queue_free()
		get_parent().mostrar_tutorial = false
		GerenciadorDeMissoes.atualizar_missao("tut_1")
		GerenciadorDeMissoes.atualizar_missao("tut_2", 1)


func _on_bru_alvo_alcancado() -> void:
		await get_tree().create_timer(2.0).timeout
		bru.parar()

		cutscene.tween_geral([bru, jogador],[0.0,2.0], true)
		await cutscene.tween.finished

		Global.Dialogo.call_deferred(diálogo1, "coisa_cai")
		await DialogueManager.dialogue_ended
		$pegar_item.queue_free()
