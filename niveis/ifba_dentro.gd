extends Node2D

@export var introducao : DialogueResource
@onready var cavaletes_interagir: Interagir = $CavaletesInteragir
@onready var pcs_prosel: Interagir = $pcs_prosel

const INTERACOES : DialogueResource = preload("res://niveis/interacoes.dialogue")
func _ready() -> void:
	GerenciadorDeMusica.tocar("if_dentro")


func _on_cavaletes_interagir_interagir() -> void:
	Global.Dialogo(introducao, "cavaletes", false, cavaletes_interagir)


func _on_pcs_prosel_interagir() -> void:
	Global.Dialogo(INTERACOES, "pcs_prosel", false, pcs_prosel)
