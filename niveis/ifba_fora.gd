extends Node2D


#@onready var balao_dialogo: CanvasLayer = $Balao_Dialogo
@export var mostrar_tutorial := true
@onready var tutorial: Node = $tutorial

# Roda antes da função ready
func _ready() -> void:
	GerenciadorDeMusica.tocar("if_fora")
	SalvarJogo.salvando.connect(func () -> void: 
		SalvarJogo.save["ifba_fora"] = {
				"mostrar_tutorial" = mostrar_tutorial,
			
				}
		)
	if SalvarJogo.validar_save("ifba_fora"):
		mostrar_tutorial = SalvarJogo.save["ifba_fora"]["mostrar_tutorial"]
	if not mostrar_tutorial:
		tutorial.queue_free()
		Global.pode_mover = true


func _on_fotinha_foto_interagir() -> void:
	var tween : Tween = create_tween()
	$gato_anim.speed_scale = 10
	$gato_anim.play("susto")
	tween.tween_property($gato_anim, "speed_scale", 1.0, 10.0)
	GerenciadorDeMissoes.atualizar_missao("fot_gat_1")
