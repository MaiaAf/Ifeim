extends Node

@export var diálogo1 : DialogueResource

var final : bool = false

@onready var apresentador: Npc = $Apresentador
@onready var npcs: Node = $npcs

@onready var bru: Npc = $turma/Bru
@onready var kay: Npc = $turma/Kay
@onready var bia: Npc = $turma/Bia


func entrou_no_auditorio(body: Node2D) -> void:
	if body is Personagem: #and GerenciadorDeMissoes.missoes_dict["tut_02"] == 1:
		#Global.carregar_cena(NIVEL_UM, false)
		Global.Dialogo(diálogo1, "acolhimento", false, apresentador)
		$turma.show()
		bia.seguir($turma/Marker2D.global_position)
		bru.seguir($turma/Marker2D2.global_position)
		kay.seguir($turma/Marker2D3.global_position)
		await DialogueManager.dialogue_ended
		$"auditório".queue_free()
		final = true
		GerenciadorDeMissoes.atualizar_missao("tut_2") # Missão concluída

		
		for i in npcs.get_children():
			if i is not Npc: return
			i.seguir(Vector2(1000, 0))
			await get_tree().create_timer(0.2).timeout
		await get_tree().create_timer(10).timeout
		npcs.queue_free()


func _on_dialogo_saida_body_entered(body: Node2D) -> void:
	if body is Personagem and final:
		for membro: Npc in [bru,bia,kay]:
			membro.animacao.flip((membro.global_position.x 
					- body.global_position.x) > 0)
		Global.Dialogo(diálogo1, "acolhimento_saida", false, bru)
		await DialogueManager.dialogue_ended
		$turma/Dialogo_Saida.queue_free()
		$Refeitorio/Parada1/Interagir.monitoring = true
			
		bia.seguir($"Refeitorio/Parada1".global_position)
		bru.seguir($"Refeitorio/Parada2".global_position)
		kay.seguir($"Refeitorio/Parada3".global_position)
