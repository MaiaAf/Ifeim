extends Node

var kay_missao_completa : bool = false
var kay_primeiro_enconto: bool = false
@export var obj_1 : String
@export var obj_2 : String
var itens_para_kay : int = 0:
	set(novo_itens_para_kay):
		itens_para_kay = novo_itens_para_kay
		if itens_para_kay == 2 and !kay_missao_completa:
			kay_missao_completa = true
			await DialogueManager.dialogue_ended
			Global.Dialogo(sala_1.diálogo, "kay_pronta")
			emit_signal("kay_missao")
			for i in (get_tree().get_nodes_in_group(obj_1) + get_tree().get_nodes_in_group(obj_2)):
				i.queue_free()
			await DialogueManager.dialogue_ended
			Transition.transicionar(2.5)
			await Transition.animation_finished
			Global.Dialogo(sala_1.diálogo, "kay_conclusão")
			await DialogueManager.dialogue_ended
			ka.criar_objeto("res://cenas/objetos/Computador.tscn")
			return

signal kay_missao


@onready var sala_1: Node = $".."
@onready var ka_foco : Marker2D = $Ka/Kay_foco
@onready var ka: Npc = $Ka

func _on_item_body_entered(body: Node2D) -> void:
	if itens_para_kay == 2:
		return
	if body.is_in_group(obj_1):
		Global.Dialogo(sala_1.diálogo, "kay_engrenagem", true, ka_foco)
		itens_para_kay +=1
		return

	if body.is_in_group(obj_2):
		Global.Dialogo(sala_1.diálogo, "kay_picareta", true, ka_foco)
		itens_para_kay +=1
		return

	if body.is_in_group("Computador"): return
	
	if body.is_in_group("Levantáveis"):
		Global.Dialogo(sala_1.diálogo, "kay_n_precisa", true, )


func _on_item_body_exited(body: Node2D) -> void:
	if body.is_in_group(obj_1) or body.is_in_group(obj_2):
		itens_para_kay -= 1


func _on_kay_conversa_interagir() -> void:
	if kay_missao_completa:
		Global.Dialogo(sala_1.diálogo, "kay_concluído", true)
		return
	
	var kay_dialogo: String
	if !kay_primeiro_enconto:
		kay_primeiro_enconto = true
		kay_dialogo = "kay_1"
	else:
		kay_dialogo = "Kay_idle"
	Global.Dialogo(sala_1.diálogo, kay_dialogo, false, ka_foco)
