extends Node

@onready var camera_cutscene: Camera2D = %Camera_cutscene
@onready var s_3_finished: Marker2D = $"../AnimationPlayers/S3_finished"
@onready var nivel_1: Node2D = $".."

# Focos de câmera
 

var diálogo : DialogueResource = Nivel_um.diálogo
var barreira : Node


var Sala_3_completa : bool = false:
	set(novo_s_3_completa):
		if novo_s_3_completa != Sala_3_completa and novo_s_3_completa:
			barreira.queue_free()
			
			GerenciadorDeMissoes.atualizar_missao("n1_sl3")
			GerenciadorDeMissoes.atualizar_missao("ni_02", 1)
			
			camera_cutscene.tween_geral([s_3_finished, null, nivel_1.jogador],[2,1, 2])
			await camera_cutscene.tween.finished
			Global.Dialogo(diálogo, "sala_tres_completa")

var Sala_3_itens : int = 2:
	set(novo_sala_1_itens):
		# usar setter para verificar se a variável é 0 toda vez que ela mudar
		Sala_3_itens = novo_sala_1_itens
		Sala_3_completa = true if Sala_3_itens == 0 else false
