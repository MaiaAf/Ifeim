extends Node

@onready var camera_cutscene: Camera2D = %Camera_cutscene
#@onready var s_1_finished: Marker2D = $"../AnimationPlayers/S1_finished"
@onready var nivel_1: Node2D = $".."
@onready var conjunto : Node = $conjunto

@onready var carrinhos_missao: Node = $"CarrinhosMissão"
@onready var coruja_missao: Node = $"CorujaMissão"

var diálogo : DialogueResource = Nivel_um.diálogo

var sala_1_completa : bool = false:
	set(novo_s_1_completa):
		if novo_s_1_completa != sala_1_completa and novo_s_1_completa:
			GerenciadorDeMissoes.atualizar_missao("n1_sl1")
			GerenciadorDeMissoes.atualizar_missao("n1_sl2", 1)
			
			#camera_cutscene.tween_geral([s_1_finished, null, nivel_1.jogador],[2,1, 2])
			camera_cutscene.tween_geral([conjunto, null, nivel_1.jogador],[2,1, 2])
			await camera_cutscene.tween.finished
			Global.Dialogo(diálogo, "sala_um_completa")
			Global.carregar_cena("res://niveis/ifba_dentro.tscn", false)
			await DialogueManager.dialogue_ended
			Global.carregar_cena("res://niveis/ifba_dentro.tscn", true, Vector2(1000, 0))


var sala_1_itens : int = 3:
	set(novo_sala_1_itens):
		# usar setter para verificar se a variável é 0 toda vez que ela mudar
		sala_1_itens = novo_sala_1_itens
		sala_1_completa = true if sala_1_itens == 0 else false
		conjunto.progresso = conjunto.quantidade - sala_1_itens # entre 0 e 3


var itens : Array = ["Computador","Roteador", "Usb"]


func _ready() -> void:
	SalvarJogo.salvando.connect(func salvar() -> void:
		SalvarJogo.save["n1_sala1"] = {
			"sala_1_completa" = sala_1_completa,
			"kay_missao_completa" = carrinhos_missao.kay_missao_completa,
			"bru_bia_segundo_encontro" = coruja_missao.bru_bia_segundo_encontro
			
		}
	)
	if SalvarJogo.validar_save("n1_sala1"):
		var save : Dictionary = SalvarJogo.save["n1_sala1"]
		sala_1_completa = save["sala_1_completa"]
		carrinhos_missao.kay_missao_completa = save["kay_missao_completa"] 
		coruja_missao.bru_bia_segundo_encontro = save["bru_bia_segundo_encontro"]


func _entrou_no_conjunto(body: Node2D) -> void:
	if not body.is_in_group("Levantáveis") or sala_1_completa:
		return
	for item : String in itens:
		if body.is_in_group(item):
			sala_1_itens -= 1
			return
	sala_1_itens += 1


func saiu_do_conjunto(body: Node2D) -> void:
	if not body.is_in_group("Levantáveis") or sala_1_completa:
		return
	for item: String in itens:
		if body.is_in_group(item):
			sala_1_itens += 1
			return
	sala_1_itens -= 1
