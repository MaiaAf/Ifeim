extends Node

@onready var barreira_s_3: StaticBody2D = $"Barreira S3"
#@onready var s_2_finished: Marker2D = $"../AnimationPlayers/S2_finished"
@onready var camera_cutscene: Camera2D = $Cutscene

var Sala_2_completa : bool = false:
	set(novo_s_2_completa):
		if novo_s_2_completa != Sala_2_completa and novo_s_2_completa:
			barreira_s_3.queue_free()
			GerenciadorDeMissoes.atualizar_missao("n1_sl2")
			GerenciadorDeMissoes.atualizar_missao("n1_sl3", 1)
			
			#camera_cutscene.tween_geral([s_2_finished, null, jogador],[2,1, 2])
			await camera_cutscene.tween.finished
			Global.Dialogo(diálogo, "sala_dois_completa")
var Sala_2_itens : int = 3:
	set(Novo_sala_2_itens):
		Sala_2_itens = Novo_sala_2_itens
		Sala_2_completa = true if Sala_2_itens == 0 else false

var jogador : Personagem
var diálogo : DialogueResource = Nivel_um.diálogo
var helb_primeiro_enconto: bool = false
var Res10 := false
var Res16 := false
var Res100 := false
var Res1600 := false

@onready var helb: Npc = $Helb

func _ready() -> void:
	jogador = get_tree().get_first_node_in_group("Player")
	$"Circuito/Lâmpada".enabled = false


func _entrou_no_conjunto_2(body: Node2D) -> void:
	if not body.is_in_group("Levantáveis") or Sala_2_completa:
		return
	if body.is_in_group("Multimetro") or body.is_in_group("Resistor16"):
		Sala_2_itens -= 1
	else:
		Sala_2_itens += 1

func _saiu_do_conjunto_2(body: Node2D) -> void:
	if body is Personagem or Sala_2_completa:
		return
	if body.is_in_group("Multimetro") or body.is_in_group("Resistor16"):
		Sala_2_itens += 1
	else:
		if body.is_in_group("Levantáveis"):
			Sala_2_itens -= 1


func _on_circuito_body_entered(body: Node2D) -> void:
	if body.is_in_group("Multimetro"):
		Global.Dialogo(diálogo, "circuito_multimetro")
	if body.is_in_group("Resistor16"):
		body.queue_free()
		$Node2D/Resistor.show()
		%n1_sl2.play("acender luz")
		Sala_2_itens -= 1


func mostrar_resistor() -> void:
	if GerenciadorDeMissoes.resistor == 0:
		return
	match GerenciadorDeMissoes.resistor:
		10:
			if !Res10:
				helb.criar_objeto("res://cenas/objetos/Computador.tscn")
				Res10 = true
		16:
			if !Res16:
				helb.criar_objeto("res://cenas/objetos/resistor_16.tscn")
				Res16 = true
		100:
			if !Res100:
				helb.criar_objeto("res://cenas/objetos/Picareta.tscn")
				Res100 = true
		1600:
			if !Res1600:
				Res1600 = true

func _on_helb_interagir() -> void:
	if !helb_primeiro_enconto:
		helb_primeiro_enconto = true
		Global.Dialogo(diálogo, "resistores")
	else:
		Global.Dialogo(diálogo, "oferecer_resistores", false, helb)
	await DialogueManager.dialogue_ended
	mostrar_resistor()
	GerenciadorDeMissoes.resistor = 0
