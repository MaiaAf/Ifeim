extends Node

@export var velocidade : int = 50


var velocidade_sapo : int = velocidade
var velocidade_gato : int = velocidade

var semáforo : bool = false

@onready var gato_progresso: PathFollow2D = $Gato_caminho/Gato_progresso
@onready var sapo_progresso: PathFollow2D = $Sapo_caminho/Sapo_progresso

@onready var sapo_anim: AnimatedSprite2D = $Sapo_caminho/Sapo_progresso/Sapo/sapo_anim
@onready var gato_anim: AnimatedSprite2D = $Gato_caminho/Gato_progresso/Gato/gato_anim
@onready var efeito: AudioStreamPlayer2D = $Sapo_caminho/Sapo_progresso/Sapo/efeito

@onready var semaforo_sprite: Sprite2D = $Sapo_caminho/Semaforo/SemaforoSprite


func _physics_process(delta: float) -> void:
	gato_progresso.progress += velocidade_gato * delta
	sapo_progresso.progress += velocidade_sapo * delta
	if !sapo_anim.is_playing():
		sapo_anim.play("Sapo andando")
	if !gato_anim.is_playing():
		gato_anim.play("Gato andando")


func _on_area_gato_body_entered(body: Node2D) -> void:
	if body.name.contains("Sapo"):
		velocidade_gato *= -1
		velocidade_sapo *= -1
		efeito.play()
		sapo_anim.play("Sapo batendo")
		gato_anim.play("Gato batendo")
		await get_tree().create_timer(0.1).timeout
		


func _on_sala_1_kay_missao() -> void:
	# Conecta manualmente os sinais quando a tarefa for completa
	%"Semaforo".connect("body_entered", _on_semáforo_body_entered)
	%"Semaforo".connect("body_exited", _on_semáforo_body_exited)
	# Desconecta o efeito de colisão entre os bichinhos
	%"Area Gato".disconnect("body_entered", _on_area_gato_body_entered)
	await Transition.metade
	semaforo_sprite.show()
	%"Semaforo/TrilhoEmbaixo".queue_free()
	gato_progresso.progress = 0
	sapo_progresso.progress = 10


func _on_semáforo_body_entered(body: Node2D) -> void:
	if body.name.contains("Sapo"):
		if semáforo:
			velocidade_sapo = 0
		semáforo = true
		if velocidade_sapo == 0:
			semaforo_sprite.frame = 2
			return
		
		return
	if body.name.contains("Gato"):
		if semáforo:
			velocidade_gato = 0
		semáforo = true
		semaforo_sprite.frame = 1


func _on_semáforo_body_exited(body: Node2D) -> void:
	if body.name.contains("Sapo") or body.name.contains("Gato"):
		semáforo = false
		velocidade_sapo = velocidade
		velocidade_gato = velocidade
		semaforo_sprite.frame = 0
