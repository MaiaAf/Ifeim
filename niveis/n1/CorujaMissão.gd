extends Node2D

@onready var sala_1: Node = $".."

var bru_bia_segundo_encontro: bool = false
var coruja_primeiro_encontro: bool = false
var coruja_dialogo2 : bool = false

@onready var bia: Npc = $Bia
@onready var bru: Npc = $Bru
@onready var bru_bia: CollisionShape2D = $Bru_e_Bia_conversa/CollisionShape2D
@onready var barreira: StaticBody2D = $"Coruja Rainha/barreira"
@onready var bru_e_bia_conversa: Area2D = $Bru_e_Bia_conversa



func _on_bru_e_bia_conversa_body_entered(body: Node2D) -> void:
	if body is Personagem:
		if coruja_primeiro_encontro:
			Global.Dialogo(sala_1.diálogo, "bru_bia_2", false, bru_bia)
			await DialogueManager.dialogue_ended
			bru_bia_segundo_encontro = true
			if GerenciadorDeMissoes.variaveis_dialogo["bru_bia_seguir"]:
				bru_e_bia_conversa.queue_free()
				bru.seguir_jogador()
				bia.seguir_jogador()
			return
		Global.Dialogo(sala_1.diálogo, "bru_bia_1")
		await DialogueManager.dialogue_ended


func _on_coruja_rainha_body_entered(body: Node2D) -> void:
	if body is Npc: body.parar()
	if body is not Personagem: return
	if coruja_dialogo2:
		return
	var coruja_dialogo : String
	if bru_bia_segundo_encontro and coruja_primeiro_encontro:
		coruja_dialogo = "coruja_2"
		coruja_dialogo2 = true

	else:
		coruja_primeiro_encontro = true
		coruja_dialogo = "coruja_1"
	Global.Dialogo(sala_1.diálogo, coruja_dialogo, false, $"Coruja Rainha")
	await DialogueManager.dialogue_ended
	if coruja_dialogo2:
		barreira.queue_free()
