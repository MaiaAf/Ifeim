extends Node2D
class_name Nivel_um

@export var pedra: Marker2D
@export var parede: Marker2D
@export var cutsene : bool = true # Debugging
var jogador : CharacterBody2D
static var diálogo : DialogueResource = preload("res://niveis/n1/nivel_1.dialogue")

@onready var sah: Npc = $Sala1/Sah
@onready var camera_cutscene: Camera2D = %Camera_cutscene


func _ready() -> void:
	jogador = get_tree().get_first_node_in_group("Player")
	GerenciadorDeMissoes.atualizar_missao("n1_sl1", 1)
	GerenciadorDeMusica.tocar("n1")
	if cutsene: await cutscene_um()


func cutscene_um() -> void:
	Global.pode_mover = false
	camera_cutscene.tween_geral(
			[null, pedra, null, jogador, null], 
			[0.5, 2.0, 1.5, 2.0, 1.0],
			)
	await camera_cutscene.tween.finished
	Global.Dialogo(diálogo, "inicio", false, sah)
	await DialogueManager.dialogue_ended
	GerenciadorDeMissoes.atualizar_missao("ni_01", 1) # Missão liberada
