extends AnimatedSprite2D

@export var foto : Texture2D
@export var dialogo : String
@export var control : Control
@export var pop_up_image: TextureRect

const FOTOS = preload("res://arte/fotos/fotos.dialogue")


var pos_inicial : Vector2
var tween : Tween
signal foto_interagir

func _ready() -> void:
	pos_inicial = control.position + Vector2(0, -200)

func _on_interagir_interagir() -> void:
	tween = get_tree().create_tween().set_trans(Tween.TRANS_CUBIC).set_ease(Tween.EASE_IN_OUT)
	var pos_final : Vector2 = control.position
	control.position = pos_inicial
	pop_up_image.texture = foto
	control.show()
	tween.tween_property(control, "global_position", pos_final, 1)
	
	Global.esconder_ui.emit(0, $CanvasLayer/Control/Voltar.pressed)
	foto_interagir.emit()



func _on_voltar_pressed() -> void:
	if dialogo:
		Global.Dialogo(FOTOS, dialogo)
		await DialogueManager.dialogue_ended
	self.queue_free()
	#_on_interagir_interagir()
