class_name Humano
extends CharacterBody2D

# estilo
@export var colocar_posicao : Marker2D
@export var animacao : Node
@export var speed : int
@export_category("Estilo")
@export var nome: String
@export var cor : int = -1
@export var cor_cabelo : int = -1
@export var cor_olho : int = -1
@export var penteado : int = -1
@export var camisa : int = -1



func criar_objeto(item: String) -> void:
	await get_tree().process_frame
	if animacao is AnimatedSprite2D: colocar_posicao.position.x = -11 if animacao.flip_h else 11
	var objeto : Node2D = load(item).instantiate()
	get_parent().add_child(objeto)
	objeto.position = colocar_posicao.global_position
