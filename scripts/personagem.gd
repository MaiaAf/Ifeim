class_name Personagem
extends Humano

@export var camera: Camera2D

static var segurando_item : bool = false

@onready var gerenciador_de_objetos: Marker2D = $GerenciadorDeObjetos
@onready var pasos: AudioStreamPlayer = $Pasos


func _ready() -> void:
	if SalvarJogo.validar_save("PlayerInfo"):
		var playerinfo : Dictionary = SalvarJogo.save["PlayerInfo"]
		nome = playerinfo["nome"]
		cor = playerinfo["cor"]
		cor_cabelo = playerinfo["cor_cabelo"] 
		cor_olho = playerinfo["cor_olho"]
		penteado = playerinfo["penteado"]
		camisa = playerinfo["camisa"]
		animacao.configurar_humano()
	
	Global.jogador_parou.connect(func (pode_mover: bool) -> void:
		if pode_mover:
			set_physics_process(true)
			return
		set_physics_process(false)
		animacao.mudar_animação("idle")
		)
	
	Global.teleportou.connect(func (teleportar_para : Vector2) -> void:
		if teleportar_para != Vector2.ZERO:
			global_position = teleportar_para
		await get_tree().create_timer(0.1).timeout
		)
	
	SalvarJogo.salvando.connect(func() -> void:
		SalvarJogo.save["Player"] = {
				"global_position": global_position,
				"speed": speed,
				"segurando_item": segurando_item,
				#"camera.global_position" : camera.global_position,
				"cena_atual": Global.cena_atual.scene_file_path
				}
		)
	if SalvarJogo.validar_save("Player"):
		carregar.call_deferred()

func carregar() -> void:
	var save : Dictionary = SalvarJogo.save["Player"]
	if Global.cena_atual.scene_file_path == save["cena_atual"]:
		global_position = save["global_position"] 
	speed = save["speed"]
	segurando_item = save["segurando_item"]
	#camera.global_position = save["camera.global_position"]

func _physics_process(_delta: float) -> void:
	movimento()


func movimento() -> void:
	var input : Vector2 = Input.get_vector("ui_left", "ui_right", "ui_up", "ui_down")
	velocity = speed * input
	move_and_slide()
	
	if velocity == Vector2.ZERO:
		animacao.mudar_animação("idle")
		
		return
	animacao.mudar_animação("andando")

	# somente com joystick, velocidade de animação baseada na força do input
	animacao.mudar_velocidade(input)


func _input(_event: InputEvent) -> void:
	if Input.get_action_strength("ui_right") != 0:
		animacao.flip(false)
		return
	elif Input.get_action_strength("ui_left") != 0:
		animacao.flip(true)

func _on_animacao_andando_passo() -> void:
	pasos.play()
