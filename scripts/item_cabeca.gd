extends Marker2D

var caminho_do_obj : String = ""
var objeto : Node

func _ready() -> void:
	SalvarJogo.salvando.connect(func () -> void:
		SalvarJogo.save["item_cabeça"] = {
			"caminho_do_obj" = caminho_do_obj,
			}
		)
	
	if SalvarJogo.validar_save("Item_cabeça"):
		var save : Dictionary = SalvarJogo.save["Item_cabeça"]
		caminho_do_obj = save["caminho_do_obj"]
		gerenciar_objeto()
	
func gerenciar_objeto() -> void:
	if Personagem.segurando_item:
		for node: Node in get_children():
			node.queue_free()
		get_parent().criar_objeto(caminho_do_obj)
	else:
		objeto = get_tree().get_first_node_in_group("item_selecionado")
		caminho_do_obj = objeto.scene_file_path
		objeto.get_node("Sprite2D").reparent(self)
		objeto.get_node("Item_Pegável").reparent(self)
		get_node("Sprite2D").position = position
		get_node("Item_Pegável").desfoco_item()
		get_node("Item_Pegável").sinalizar(1)
		objeto.queue_free()
		#Global.botao_acao.emit(1)
	Personagem.segurando_item = not Personagem.segurando_item
