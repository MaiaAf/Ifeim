class_name NpcSpawner
extends Node2D

@export var npcs : int = 15
@export var alcance : int = 300
const NPC : PackedScene = preload("res://cenas/npc.tscn")

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	criar_npcs.call_deferred()
	y_sort_enabled = true


func criar_npcs() -> void:
	for npc in npcs:
		var instancia_npc : Npc = NPC.instantiate()
		add_child(instancia_npc)
		instancia_npc.position = Vector2(randi_range(-alcance,alcance), randi_range(-alcance,alcance))
