class_name Barramento
extends Node

## Sinal para avisar o jogador para mudar a posição no mapa,
## utilizado com a função [code]carregar_cena()[/code]
signal teleportou(tp : Vector2)
## Sinal para impedir o movimento do jogador, utilizado por [code]Dialogo()[/code] e cutscenes
@warning_ignore("unused_signal")
signal jogador_parou(jp : bool)
## Sinal para esconder a interface de usuário em diferentes níveis,
## utilizado por [code]Dialogo()[/code] e cutscenes
signal esconder_ui(parâmetro : int, sinal : Signal)
## Mediador entre o módulo de interação e o botão de interagir na UI,
## envia a textura do interativo e uma referência ao objeto
@warning_ignore("unused_signal")
signal botao_acao(textura: int, interativo : Node)

# Teleporte e movimento
var cena_atual : Node = null
var pode_mover : bool = true:
	set(novo_pode_mover):
		pode_mover = novo_pode_mover
		emit_signal("jogador_parou", pode_mover)


func _ready() -> void:
	self.process_mode = Node.PROCESS_MODE_ALWAYS
	cena_atual = get_tree().current_scene


## Carrega a cena indicada se [code]mudar = false[/code] (para uma mudança rápida quando for preciso),
## remove a cena atual (não altera autoloads) e substitui pela cena indicada se [code]mudar = true[/code].
## Também possibilita especificar a localização do Persongem na nova cena.
func carregar_cena(res_path : String, mudar : bool = true, localização: Vector2 = Vector2.ZERO, trans : bool = true) -> void:
	await get_tree().process_frame
	
	if ResourceLoader.load_threaded_get_status(res_path) == 0:
		ResourceLoader.load_threaded_request(res_path)
		if not mudar:
			return
			# Carregar a cena na memória com antecedência e retornar
	
	var cena: Resource = ResourceLoader.load_threaded_get(res_path)
	if trans:
		Transition.transicionar()
		await get_tree().create_timer(Transition.duracao_segundos / 2).timeout
	cena_atual.free()
	cena_atual = cena.instantiate()
	get_tree().root.add_child(cena_atual)
	get_tree().current_scene = cena_atual
	teleportou.emit(localização)


## Essa função recebe um arquivo de .dialogue, o título do diálogo e se o jogador poderá se mover ou não durante o diálogo
func Dialogo(diálogo : DialogueResource, titulo : String, movimento : bool = false, npc : Node2D = null) -> void:
	esconder_ui.emit(1, DialogueManager.dialogue_ended)
	pode_mover = movimento
	DialogueManager.show_dialogue_balloon(diálogo, titulo)
	if npc != null:
		var camera : Cutscene = get_tree().get_first_node_in_group("Cutscene")
		camera.tween_dialogo(npc)
