extends Node

var salvar_automaticamente : bool = false:
	set(n_salvar_automaticamente):
		salvar_automaticamente = n_salvar_automaticamente
		autosave()
	

const CAMINHO : String = "user://save.data"

var save : Dictionary = {}
var timer : Timer
signal salvando
signal jogo_salvo
signal carregando
#signal sem_saves

func _ready() -> void:
	carregar_jogo()
	
		# Cria um cronômetro que salva automaticamente
	timer = Timer.new()
	add_child(timer)
	timer.wait_time = 40
	timer.timeout.connect(func () -> void: salvar_jogo())
	autosave()

func salvar_jogo() -> void:
	salvando.emit()
	salvar_deferred.call_deferred()
	# Garante que a função seja chamada no final do frame

func salvar_deferred() -> void:
	var file : FileAccess = FileAccess.open(CAMINHO, FileAccess.WRITE)
	file.store_var(save)
	file.close()
	jogo_salvo.emit()
	print("jogo salvo!")


func carregar_jogo() -> void:
	if not FileAccess.file_exists(CAMINHO):
		return
	var file : FileAccess = FileAccess.open(CAMINHO, FileAccess.READ)
	save = file.get_var()
	file.close()
	#carregando.emit()

func validar_save(key: String) -> bool:
	if not FileAccess.file_exists(CAMINHO): return false
	return true if save.has(key) else false

func resetar_save() -> void:
	var dir : DirAccess = DirAccess.open("user://")
	dir.remove(CAMINHO)
	save = {}


## Salvar informações separadas da progressão do jogo, como configurações
## Em arquivos diferentes
func salvar_conf(caminho : String, conf : Dictionary) -> void:
	var file : FileAccess = FileAccess.open(caminho, FileAccess.WRITE)
	file.store_var(conf)
	file.close()


func autosave() -> void:
	if salvar_automaticamente:
		timer.start()
	else:
		timer.stop()
