extends Node

signal instanciar_missao(botao: String, descricao: String)
@warning_ignore("unused_signal")
signal mudar_descricao

# 0 -> completa
# 1 -> incompleta
# 2 -> bloqueada

# tut -> tutorial
# ni -> nível

var missoes_dict : Dictionary = {
		"tut_1" : 2, # Tutorial
		"tut_2" : 2,

	# Desafios
		"n1_sl1": 2,
		"n1_sl2": 2,
		"n1_sl3": 2,
		"ni_01" : 2,
		
		"ni_02": 2,

	# Fotos
		"fot_gat_1" : 1,
		"fot_gat_2" : 2,
		
}

var variaveis_dialogo : Dictionary = {
	# Refeitório
	"ver_lab4" : false,
	"seguir_refeitorio" : false,
	
	# Nível 1, Sala 1
	"bru_bia_seguir" : false,
	
	
}
#var Nível1: bool = false
var resistor : int


var Nível2: bool = false
var Nível3: bool = false
var Nível4: bool = false
var Nível5: bool = false


func _ready() -> void:
	carregar_missoes()
	SalvarJogo.salvando.connect(func () -> void:
		SalvarJogo.save["missoes_dict"] = missoes_dict
		)

func carregar_missoes() -> void:
	if SalvarJogo.validar_save("missoes_dict"):
		missoes_dict = SalvarJogo.save["missoes_dict"]
	ui_mostrar_missoes()


func atualizar_missao(missao : String, valor : int = 0) -> void:
	var missoes : Array[Node] = get_tree().get_nodes_in_group("ui_botao_mi")
	SalvarJogo.salvar_jogo()
	# Remover missões instanciadas na interface gráfica
	for node: ui_botao_missao in missoes:
		node.queue_free()
	missoes_dict[missao] = valor
	ui_mostrar_missoes()
	if valor == 1:
		var mensagem : String = tr("notif_" + missao) if tr("notif_" + missao) != ("notif_" + missao) else "notif"
		get_tree().get_first_node_in_group("GerenciadorDeUi").notificar(mensagem)

func ui_mostrar_missoes() -> void:
	for missao: String in missoes_dict:
		if missoes_dict[missao] == 1:
			instanciar_missao.emit(tr(missao), tr("des_" + missao))

func is_missao_completa(missao : String) -> bool:
	return missoes_dict[missao] == 0
