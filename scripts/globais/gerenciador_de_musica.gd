extends AudioStreamPlayer

func tocar(clip : String) -> void:
	if not playing:
		playing = true
	get_stream_playback().switch_to_clip_by_name(clip)
