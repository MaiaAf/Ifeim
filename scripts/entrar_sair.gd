extends Interagir

#@export var personagem = CharacterBody2D
@export_category("Marcadores")
@export var porta_destino : Vector2
@export var próxima_cena : String
@export var carregar_ao_entrar : bool = false

func _on_body_entered(body: Node2D) -> void:
	if body is Personagem and carregar_ao_entrar: 
		Global.carregar_cena(próxima_cena, false)
	#if body.is_in_group("npc"):
		#body.queue_free()


func _on_interagir() -> void:
	Global.carregar_cena(próxima_cena, true, porta_destino)
