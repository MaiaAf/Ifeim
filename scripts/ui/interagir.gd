class_name Interagir
extends Area2D

@export_enum('levantar', 'abaixar', 'conversar', 'entrar', 'pc', 'fotinha') var textura : int = 2
@export var desativar : bool = false
static var fila : Array[Interagir]
@warning_ignore("unused_signal")
signal interagir


func _ready() -> void:
	collision_mask = 2
	body_entered.connect(_on_body_entered_interagir)
	body_exited.connect(_on_body_exited_interagir)


func sinalizar(_textura: int = textura) -> void:
	Global.botao_acao.emit(_textura, self)
	# Uma referência à instância atual é passada para que o receptor 
	# do sinal possa emitir o sinal interagir quando o botão for apertado


func _on_body_entered_interagir(body: Node2D) -> void:
	if desativar: return
	if body is Personagem:
		fila.append(self)
		if !fila.is_empty():
			var numeros: Array[int] = []
			for i in fila:
				numeros.append(i.textura)
			if self.textura > numeros.min():
				return
		sinalizar()


func _on_body_exited_interagir(body: Node2D) -> void:
	if desativar: return
	if body is Personagem:
		fila.erase(self)
		if fila.is_empty():
			sinalizar(-1)
			return
		var proximo_interagir : Interagir = fila[0]
		for i in fila:
			if i.textura < proximo_interagir.textura:
				proximo_interagir = i
		proximo_interagir.sinalizar()
