extends CanvasLayer

@export var menu_1: Control
@export var menu_2: MarginContainer


func _on_configuraçõs_pressed() -> void:
	menu_1.hide()
	menu_2.show()


func _on_menu_pressed() -> void:
	hide()
	get_tree().paused = false
	SalvarJogo.save["header"] = {
	"data_hora" = Time.get_date_string_from_system(),
	"current_scene" = get_tree().current_scene.scene_file_path,
	}
	SalvarJogo.salvar_jogo()
	Global.carregar_cena("res://cenas/interfaces/menu.tscn")


func _on_continuar_pressed() -> void:
	self.hide()
	get_tree().paused = false


func _on_configurações_voltar() -> void:
	menu_1.show()
	menu_2.hide()
