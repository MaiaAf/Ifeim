extends Control

@export var menu_1: Control
@export var menu_2: Control

const IFBA_FORA : String = "res://niveis/ifba_fora.tscn"
var cena_inicial : String = IFBA_FORA

@onready var novo_jogo: Button = $"Menus/Menu_1/VBoxContainer/NovoJogo"
@onready var criacao_de_personagem: PanelContainer = $Menus/CriacaoDePersonagem
#@onready var criacao_de_personagem: PanelContainer = $Menus/MarginContainer/CriacaoDePersonagem


func _ready() -> void:
	GerenciadorDeMusica.tocar("menu")
	novo_jogo.hide()
	carregar()


func _on_continuar_pressed() -> void:
	if not SalvarJogo.validar_save("header"):
		criacao_de_personagem.show()
		return
	carregar(true)

func _on_configuraçõs_pressed() -> void:
	menu_1.hide()
	menu_2.show()


func _on_sair_pressed() -> void:
	get_tree().quit()


func _on_button_toggled(_button_pressed: bool) -> void:
	Global.tela_cheia()


func _on_configurações_voltar() -> void:
	menu_2.hide()
	menu_1.show()


func carregar(iniciar_jogo : bool = false) -> void:
	if iniciar_jogo:
		Global.carregar_cena(cena_inicial)
		Global.pode_mover = true
		return
	# Se o header existe, define a cena do jogo anterior
	# e mostra o botão de novo jogo
	if SalvarJogo.validar_save("header"):
		var header : Dictionary = SalvarJogo.save["header"] 
		cena_inicial = header["current_scene"]
		novo_jogo.show()
	# Carrega a cena inicial na memória
	Global.carregar_cena(cena_inicial, false)


func _on_novo_jogo_pressed() -> void:
	SalvarJogo.resetar_save()
	cena_inicial = IFBA_FORA
	#Global.carregar_cekna(cena_inicial, false)
	criacao_de_personagem.show()


func _on_criacao_de_personagem_confirmar() -> void:
	carregar(true)
