class_name GerenciadorDeUi
extends CanvasLayer

const MARGEM : float = 0.1
var tween : Tween
var tween2 : Tween

@onready var virtual_joystick: Control = %"Virtual Joystick"
@onready var gerenciador_de_missoes: Control = $GerenciadorDeMissoes
@onready var btn_pausar: Button = $Jogador_ui/VBoxContainer2/VBoxContainer/Btn_pausar
@onready var pausar_ui: CanvasLayer = $Pausar_ui
@onready var pc_interface: MarginContainer = $PcInterface
@onready var notificacao: Label = $Jogador_ui/VBoxContainer2/Control/Notificacao
@onready var missao_excl: Label = $"Jogador_ui/VBoxContainer2/VBoxContainer/Missões/MissaoExcl"

static var pc: MarginContainer


func _ready() -> void:
	Global.esconder_ui.connect(_esconder_ui)
	pc = pc_interface
	add_to_group("GerenciadorDeUi")

func _esconder_ui(parâmetro: int, sinal: Signal) -> void:
	var escopo : Node
	match parâmetro:
		0: # Esconder toda interface
			escopo = self
		1: # Esconder apenas o joystick
			escopo = virtual_joystick
		2:
			pass

	escopo.hide()
	await sinal #sinal que reverterá a mudança
	escopo.show()
	virtual_joystick.show()

func _on_btn_pausar_pressed() -> void:
	pausar_ui.show()
	Global.esconder_ui.emit(1, pausar_ui.visibility_changed)
	if gerenciador_de_missoes.visible:
		gerenciador_de_missoes.visible = false
	get_tree().paused = true

func _input(_event: InputEvent) -> void:
	if Input.is_action_just_pressed("ui_cancel"):
		btn_pausar.pressed.emit()


func _on_missões_pressed() -> void:
	gerenciador_de_missoes.visible = !gerenciador_de_missoes.visible
	if gerenciador_de_missoes.visible:
		Global.esconder_ui.emit(1, gerenciador_de_missoes.visibility_changed)

static func definir_ancoras(node : Control, x_ancora : float = MARGEM, y_ancora : float = MARGEM) -> void:
	node.anchor_left = x_ancora
	node.anchor_top = y_ancora
	node.anchor_right = 1 - x_ancora
	node.anchor_bottom = 1 - y_ancora

static func mostrar_pc_int() -> void:
	pc.show()
	Global.esconder_ui.emit(1, pc.visibility_changed)
	
func notificar(mensagem : String) -> void:
	var pos : Vector2 = notificacao.position
	notificacao.text = tr(mensagem)
	reset_tween()
	
	notificacao.modulate = Color.hex(0xffffff00)
	notificacao.show()
	missao_excl.show()
	tween.tween_property(notificacao, "modulate", Color.hex(0xffffffff), 1)
	tween2.tween_property(missao_excl, "modulate", Color.hex(0xffffffff), 1)
	tween2.parallel().tween_property(missao_excl, "rotation_degrees", -15, 0.5)
	for i in range(3):
		tween2.tween_property(missao_excl, "rotation_degrees", 15, 0.5)
		tween2.tween_property(missao_excl, "rotation_degrees", -15, 0.5)
	tween2.tween_property(missao_excl, "rotation_degrees", 0, 0.5)
	await get_tree().create_timer(4).timeout
	tween2.stop()
	tween2.tween_property(notificacao, "position", Vector2(0, -10), 1)
	reset_tween()
	tween.tween_property(notificacao, "modulate", Color.hex(0xffffff00), 1)
	tween.parallel().tween_property(missao_excl, "modulate", Color.hex(0xffffff00), 1)
	
	await tween.finished
	
	notificacao.hide() 
	notificacao.position = pos
	missao_excl.rotation_degrees = 0
	missao_excl.hide()
	
func _on_album_pressed() -> void:
	var album : Resource = load("res://cenas/interfaces/album.tscn")
	add_child(album.instantiate())

func reset_tween() -> void:
	tween = get_tree().create_tween().set_trans(Tween.TRANS_CUBIC).set_ease(Tween.EASE_IN_OUT)
	tween2 = get_tree().create_tween().set_trans(Tween.TRANS_CUBIC).set_ease(Tween.EASE_IN_OUT)
