extends Camera2D

@onready var marker_2d: Marker2D = $".."


func _physics_process(_delta: float) -> void:
	var pos_mouse : Vector2 = get_viewport().get_mouse_position()
	marker_2d.global_position = pos_mouse - get_viewport_rect().size / 2
