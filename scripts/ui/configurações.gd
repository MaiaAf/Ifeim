extends MarginContainer

signal voltar

@export var faixa_de_áudio : String
@export var tela_cheia_button: CheckButton 


var bus_index: int
var config_caminho : String = "user://config"

@export var volume: HSlider

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	#DisplayServer.window_set_vsync_mode(0) # Para debug
	GerenciadorDeUi.definir_ancoras(self)
	carregar()
	SalvarJogo.salvando.connect(func () -> void:
		# Executada ao salvar
		var config_dict : Dictionary = {
			"volume" = db_to_linear(AudioServer.get_bus_volume_db(bus_index)),
			"tela_cheia" = tela_cheia_button.button_pressed,
		}
		SalvarJogo.salvar_conf(config_caminho, config_dict)
	)
		# Carregar as configurações salvas
		
	# Mudar volume como ensinado por:
	# github.com/theshaggydev/the-shaggy-dev-projects
	bus_index = AudioServer.get_bus_index(faixa_de_áudio)
	volume.value = db_to_linear(
		AudioServer.get_bus_volume_db(bus_index)
	)

func carregar() -> void:
	if not FileAccess.file_exists(config_caminho):
		return
	var file : FileAccess = FileAccess.open(config_caminho, FileAccess.READ)
	var save : Dictionary = file.get_var()
	file.close()
	
	volume.value = save["volume"]
	tela_cheia_button.button_pressed = save["tela_cheia"]


func _on_tela_cheia_button_toggled(toggled_on: bool) -> void:
	tela_cheia(toggled_on)
	
func tela_cheia(toggled_on: bool = true) -> void:
	var mode : int = 4 if toggled_on else 0
	DisplayServer.window_set_mode(mode)


func _input(_event: InputEvent) -> void:
	# Altera entre tela cheia e em janela
	if Input.is_action_just_pressed("fullscreen"):
		tela_cheia_button.button_pressed = not tela_cheia_button.button_pressed


func _on_voltar_pressed() -> void:
	# Salvar configurações e voltar para o menu
	SalvarJogo.salvar_jogo()
	voltar.emit()


func _on_volume_value_changed(value: float) -> void:
	AudioServer.set_bus_volume_db(
	bus_index,
	linear_to_db(value)
	)
	
	


func _on_option_button_item_selected(index: int) -> void:
	var language : String
	match index:
		0:
			language = "pt_BR"
		1:
			language = "en"
	TranslationServer.set_locale(language)


func _on_vsync_toggled(toggled_on: bool) -> void:
	var vsync : int = 0 if toggled_on else 1
	DisplayServer.window_set_vsync_mode(vsync)
