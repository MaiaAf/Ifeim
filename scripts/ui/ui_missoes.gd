extends PanelContainer

var descricao_da_missao : String
@onready var ui_espaco_1: Control = $MarginContainer/HBoxContainer/PainelLateral/Ui_espaco1


func _ready() -> void:
	GerenciadorDeMissoes.instanciar_missao.connect(func _on_missao_instanciada(botao: String, descricao: String) -> void:
		var botao_missao : Button = load("res://componentes/botao_missao.tscn").instantiate()
		ui_espaco_1.add_sibling(botao_missao)
		botao_missao.text = botao
		botao_missao.descricao = descricao
		)
	GerenciadorDeMissoes.carregar_missoes()
	var primeira_missao: Button = get_tree().get_first_node_in_group("ui_botao_mi")
	GerenciadorDeMissoes.mudar_descricao.connect(func _on_mudar_descricao(descricao: String) -> void:
		%"DescriçãoDaMissão".text = descricao
	)
	if not is_instance_valid(primeira_missao): return
	primeira_missao.button_pressed = true
	%"DescriçãoDaMissão".text = primeira_missao.descricao
