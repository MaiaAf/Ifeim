extends Button

var textura_atual : int
var responder_interativo : Interagir

func _ready() -> void:
	hide()
	Global.botao_acao.connect(func (textura: int, interativo : Interagir) -> void:
			responder_interativo = interativo
			textura_atual = textura
			mudar_textura()
			)


func _on_botao_pressed() -> void:
	responder_interativo.interagir.emit()
	# Emitir o sinal

func mudar_textura() -> void:
	if textura_atual == -1:
		hide()
		return
	show()
	icon.region = Rect2(textura_atual * 32, 0, 32, 32)
