class_name Cutscene
extends Camera2D

var jogador : CharacterBody2D
var tween : Tween


func _ready() -> void:
	jogador = get_tree().get_first_node_in_group("Player")
	self.enabled = false
	self.add_to_group("Cutscene")

func reset_tween() -> void:
	tween = get_tree().create_tween().set_trans(Tween.TRANS_CUBIC).set_ease(Tween.EASE_IN_OUT)

## Inicia a câmera para as outras funções (ativar = true) ou entrega o controle de volta para a câmera
## do jogador (ativar = false) 
func ativar_camera(ativar: bool = true, reset_pos : bool = true, ui : bool = true) -> void:
	if ativar:
		reset_tween()
		self.enabled = true
		if reset_pos: self.position = jogador.position
		make_current()
	else:
		await tween.finished
		jogador.camera.make_current()
		self.enabled = false
	if not ui: Global.esconder_ui.emit(0, tween.finished)


## Recebe dois arrays, um de posições (Vector2) e outro de tempo (float).
## A função move a câmera para cada posição em seu tempo correspondente, baseado no índice.
## A primeira animação moverá a câmera para a primeira posição com a
## duração do primeiro membro do array tempo, em segundos.
func tween_geral(posicoes: Array, tempo: Array, reset_pos : bool = true) -> void:
	
	ativar_camera(true, reset_pos)
	Global.esconder_ui.emit(0, tween.finished)

	# Se a posição não for Vector2, espera o tempo de tempo[i]
	for index : int in posicoes.size():
		if posicoes[index] != null:
			tween.tween_property(self, "position", posicoes[index].global_position, tempo[index])
		else:
			tween.tween_interval(tempo[index])
	ativar_camera(false)


## Centraliza a cãmera no Npc determinado, até que o diálogo termine.
func tween_dialogo(npc: Node2D, esperar : Signal = DialogueManager.dialogue_ended) -> void:
	ativar_camera()
	tween.tween_property(self, "position", npc.global_position, 1)
	await esperar
	reset_tween()
	jogador.camera.global_position = jogador.global_position
	tween.tween_property(self, "position", jogador.camera.global_position, 1.0)
	ativar_camera(false)

## Zoom in e Zoom out
func tween_zoom(novo_zoom : Vector2, tempo: float) -> void:
	reset_tween()
	tween.tween_property(self, "zoom", novo_zoom, tempo)
	
