extends Control

@export var conceitos: int
@onready var control: VBoxContainer = $"MarginContainer/TabContainer/Conceitos das fases/HBoxContainer/PainelLateral/ScrollContainer/Control"

var tema_btn : Resource = preload("res://cenas/utilidades/pc_interface_btn.tres")

func _ready() -> void:
	GerenciadorDeUi.definir_ancoras(self, 0.03)
	for i in conceitos:
		var btn: Button = Button.new()
		var texto : String = "conceito" + str(i+1)
		var descricao : String = texto + "_des"
		
		btn.theme = tema_btn
		btn.text = tr(texto)
		
		control.add_child(btn)
		
		btn.pressed.connect(func () -> void:
				%"DescriçãoDoConceito".text = descricao
				)

func _on_voltar_pressed() -> void:
	hide()
