class_name ItemPegável
extends Interagir

@export var nome_do_objeto : String
@onready var item_etiqueta : Label = $item_etiqueta
var parent : RigidBody2D
static var ultimo: ItemPegável

func _ready() -> void:
	parent = get_parent()
	parent.add_to_group("Levantáveis")
	parent.add_to_group(nome_do_objeto)
	item_etiqueta.hide()
	item_etiqueta.text = nome_do_objeto
	


func _on_interagir() -> void:
	var jogador: Personagem = get_tree().get_first_node_in_group("Player")
	jogador.gerenciador_de_objetos.gerenciar_objeto()
	# A instância deste módulo é reparentada para o gereniador de objetos no personagem


func _on_body_entered_item(body: Node2D) -> void:
	if body is Personagem:
		textura = 1 if Personagem.segurando_item else 0
		_on_body_entered_interagir(body)
		if is_instance_valid(ultimo):
			ultimo.desfoco_item()
		if textura == 1: return
		foco_item()


func _on_body_exited_item(body: Node2D) -> void:
	if body is Personagem:
		textura = 1 if Personagem.segurando_item else 0
		_on_body_exited_interagir(body)
		desfoco_item()
		for i in fila:
			if i is ItemPegável:
				i.foco_item()
				return


func foco_item() -> void:
	if not is_instance_valid(parent) or Personagem.segurando_item: return
	ItemPegável.ultimo = self
	parent.add_to_group("item_selecionado")
	parent.material = preload("res://scripts/shaders/materiais/contorno.tres")
	item_etiqueta.show()


func desfoco_item() -> void:
	if not is_instance_valid(parent) or Personagem.segurando_item: return
	parent.remove_from_group("item_selecionado")
	parent.material = null
	item_etiqueta.hide()
