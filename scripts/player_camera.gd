extends Camera2D

@onready var mouse: Area2D = $"../Mouse"
@onready var collision_shape_2d: CollisionShape2D = $"../Mouse/CollisionShape2D"

var seguir_mouse : bool = false
var tamanho_da_tela: Vector2 

func _ready() -> void:
	tamanho_da_tela = get_viewport_rect().size
	collision_shape_2d.shape.size = tamanho_da_tela * 80/100


func _physics_process(_delta: float) -> void:
	if seguir_mouse:
		var pos_mouse : Vector2 = get_viewport().get_mouse_position()
		position = (pos_mouse - tamanho_da_tela /2) / 8


func _on_mouse_mouse_exited() -> void:
	seguir_mouse = true


func _on_mouse_mouse_entered() -> void:
	seguir_mouse = false
	global_position = $"..".global_position
