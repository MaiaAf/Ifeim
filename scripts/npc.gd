class_name Npc
extends Humano

@export var navigation_agent: NavigationAgent2D
## Permanecer no estado atual ou alternar aleatóriamente
@export var travar_estado : bool = false:
	set(n_travar):
		if travar_estado and not n_travar:
			_on_timer_estado_timeout()
		travar_estado = n_travar
		speed = 60 if travar_estado else 50

## 0: Parado
## 1: Andando
## 2: Seguir jogador
@export var estado : int = 1
@export var timer_estado: Timer
@export var timer_caminho : Timer

var jogador : Personagem
var seguindo_jogador: bool
var próximo_destino: Vector2
var margem : Vector2

signal alvo_alcancado
## Alvo para seguir
@export var alvo : Vector2
## Virar no eixo y
@export var flip : bool


func _ready() -> void:
	add_to_group("npc")
	if nome != "aleatorio":
		animacao.configurar_humano()
	if travar_estado:
		animacao.flip(flip)
		margem = Vector2(randi_range(-5,5), randi_range(-10,10))
	_on_timer_caminho_timeout()
	
func navigation_query() -> void:
	await get_tree().physics_frame
	if navigation_agent.is_navigation_finished():
		if !travar_estado: _on_timer_estado_timeout()

func _physics_process(_delta : float) -> void:
	navigation_query.call_deferred()

	if velocity == Vector2.ZERO:
		animacao.mudar_animação("idle")
	else:
		animacao.mudar_animação("andando")
		animacao.flip(velocity.x < 0)
	
	if seguindo_jogador:
		animacao.flip((global_position.x - próximo_destino.x) > 0) # Se virar para o personagem
		if global_position.distance_to(jogador.global_position) < 25.0 + margem.x:
			velocity = Vector2.ZERO # parar
			animacao.mudar_animação("idle")
		else:
			velocity = global_position.direction_to(próximo_destino) * speed
			_on_navigation_agent_2d_velocity_computed(velocity)
	
	if navigation_agent.avoidance_enabled:
		navigation_agent.set_velocity(velocity)
		animacao.mudar_velocidade(velocity)

	move_and_slide()


## Escolhe um estado aleatório (entre andar e ficar parado) e aplica as mudanças relevantes,
## incluindo o tempo para nova troca de estado.
func _on_timer_estado_timeout() -> void:
	if travar_estado: 
		navigation_agent.avoidance_enabled = false if estado == 0 else true
		return
	estado = randi_range(0,1)
	
	var tempos : Array[int]
	if estado == 0: 
		tempos = [4,10]
		if is_physics_processing(): set_physics_process(false)
	else:
		if not is_physics_processing(): set_physics_process(true)
		tempos = [10,30]
		alvo = self.global_position + Vector2(randi_range(-500, 500), randi_range(-500, 500))

	timer_estado.wait_time = randi_range(tempos[0],tempos[1])


## Aplica alterações segundo o estado atual e
## atualiza o caminho se o NPC estiver andando ou seguindo.
func _on_timer_caminho_timeout() -> void:
	próximo_destino = navigation_agent.get_next_path_position()
	match estado:
		0: # parado
			animacao.mudar_animação("idle")
			velocity = Vector2.ZERO
			await get_tree().physics_frame
			return
		1: # andando
			velocity = global_position.direction_to(próximo_destino) * speed
		2: # seguir jogador
			seguindo_jogador = true
			alvo = jogador.global_position + margem
			
	await get_tree().physics_frame
	navigation_agent.target_position = alvo


func _on_navigation_agent_2d_target_reached() -> void:
	if travar_estado:
		if estado == 2: return
		estado = 0
		alvo_alcancado.emit()


func _on_navigation_agent_2d_velocity_computed(safe_velocity: Vector2) -> void:
	velocity = safe_velocity

## Faz o NPC ir para determinada posição, podendo ficar preso nesse estado ou não
func seguir(_alvo: Vector2, _travar_estado : bool = true) -> void:
	travar_estado = _travar_estado
	estado = 1
	alvo = _alvo
	_on_timer_caminho_timeout()

## Faz o NPC seguir o jogador
func seguir_jogador() -> void:
	jogador = get_tree().get_first_node_in_group("Player")
	estado = 2
	timer_caminho.wait_time = 0.2
	_on_timer_caminho_timeout()

## Faz o NPC parar, podendo permanecer parado ou não (depende do TimerEstado caso `_travar_estado = false`)
func parar(_travar_estado : bool = true) -> void:
	travar_estado = _travar_estado
	estado = 0
	timer_caminho.wait_time = 1
	_on_timer_caminho_timeout()
