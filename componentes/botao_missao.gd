class_name ui_botao_missao
extends Button


@export var descricao: String


func _on_pressed() -> void:
	GerenciadorDeMissoes.mudar_descricao.emit(descricao)
	for node : ui_botao_missao in get_tree().get_nodes_in_group("ui_botao_mi"):
		if node != self:
			node.button_pressed = false
