extends Control

@export var foto : Texture

@onready var texture_rect: TextureRect = %TextureRect

func _ready() -> void:
	texture_rect.texture = foto
