extends Node2D

signal anim_mudada
signal andando_passo

var cor : int = -1
var cor_cabelo : int = -1
var cor_olho : int = -1
var penteado : int = -1
var camisa : int = -1

var cores_pele : Array[Array] = [
	[Color.hex(0x3a281e_ff), Color.hex(0x251811_ff)], # 0) escuro1
	[Color.hex(0x604535_ff), Color.hex(0x3a281e_ff)], # 1) escuro2
	[Color.hex(0xa97550_ff), Color.hex(0x835a3c_ff)], # 2) medio1
	[Color.hex(0xacd8f62_ff), Color.hex(0xaa7a5a_ff)], # 3) medio2
	[Color.hex(0xffc09a_ff), Color.hex(0xffae6e_ff)], # 4) claro1
	[Color.hex(0xffd8c4_ff), Color.hex(0xffc09a_ff)], # 5) claro2
]

var cores_cabelo : Array[Array] = [
	[Color.hex(0xfac90c_ff), Color.hex(0xd8c308_ff)], # 0) loiro
	[Color.hex(0xfc6629_ff), Color.hex(0xd36d0b_ff)], # 1) ruivo
	[Color.hex(0x94401b_ff), Color.hex(0x783b0e_ff)], # 2) castanho claro
	[Color.hex(0x6e3a27_ff), Color.hex(0x422313_ff)], # 3) castanho escuro
	[Color.hex(0x190f0e_ff), Color.hex(0x000000_ff)], # 4) escuro
]

var cores_olho : Array[Color] = [
	Color.hex(0x000000_ff), # 0) Preto
	Color.hex(0x1a1000_ff), # 1) Castanho
	Color.hex(0x2e86c5_ff), # 2) Azul
	Color.hex(0x0b7550_ff), # 3) Verde
	# Algumas cabelo de cabelo
	Color.hex(0x94401b_ff), 
	Color.hex(0x6e3a27_ff), 
	Color.hex(0x190f0e_ff), 
]

@onready var cabelo: Sprite2D = $cabelo
@onready var torso: Sprite2D = $torso
@onready var pernas: Sprite2D = $torso/pernas
@onready var rosto: Sprite2D = $rosto


func _ready() -> void:
	cor = randi_range(0, cores_pele.size() - 1)
	cor_cabelo = randi_range(0, cores_cabelo.size() - 1)
	cor_olho = randi_range(0, cores_olho.size() - 1)
	penteado = randi_range(0, 5)
	camisa = 0

	configurar_anim()
	definir_cores()

func configurar_humano() -> void:
	var pai : Humano
	pai = get_parent()
	cor = pai.cor
	cor_cabelo = pai.cor_cabelo
	cor_olho = pai.cor_olho
	penteado = pai.penteado
	camisa = pai.camisa
	
	configurar_anim()
	definir_cores()


func definir_cores() -> void:
	# Garantir que a cor selecionada nunca exceda o tamanho do array
	cor = cor % cores_pele.size()
	cor_cabelo = cor_cabelo % cores_cabelo.size()
	cor_olho = cor_olho % cores_olho.size()
	
	substituir_cor("0", cores_pele[cor][0])
	substituir_cor("1", cores_pele[cor][1])
	substituir_cor("2", cores_cabelo[cor_cabelo][0])
	substituir_cor("3", cores_cabelo[cor_cabelo][1])
	substituir_cor("4", cores_olho[cor_olho])

func substituir_cor(paramm: String, color : Color = Color(randf(), randf(), randf()))  -> void:
	material.set_shader_parameter("replace_" + paramm, color)


func ao_mudar_animação(anim: String) -> void:
	configurar_anim.call_deferred(anim)
	anim_mudada.emit()

func anim_atual() -> String:
	return %Animacoes.current_animation


func configurar_anim(anim : String = %Animacoes.current_animation) -> void:
	penteado = penteado % 5
	camisa = camisa % 4
	
	match anim:
		"idle":
			cabelo.region_rect = Rect2(128, penteado * 16, 176,16)
			cabelo.hframes = 11
			torso.region_rect =  Rect2(192, camisa * 16, 144,16)
			torso.hframes = 9
			
		"andando":
			cabelo.region_rect = Rect2i(0, penteado * 16, 128,16)
			cabelo.hframes = 8
			torso.region_rect = Rect2i(0, camisa * 16, 208,16)
			torso.hframes = 13

func mudar_animação(anim : String) -> void:
	%Animacoes.play(anim)
	
func flip(virar : bool) -> void:
	cabelo.flip_h = virar
	pernas.flip_h = virar
	torso.flip_h = virar
	rosto.flip_h = virar

func mudar_velocidade(speed : Vector2) -> void:
	var anim_velocidade : float = abs(speed.x) + abs(speed.y)
	%Animacoes.speed_scale = clamp(anim_velocidade, 0.35, 1.0)

func passo() -> void:
	andando_passo.emit()
