# IFEIM

## Criação de um jogo utilizando apenas programas livres e gratuitos:


![Protótipo de menu / menu prototype](./.gitlab/Menu.webp)

O processo de desenvolvimento de um jogo requer a articulação entre código e diversas modalidades artísticas como música, efeitos sonoros, modelagem 3D e texturas; requirindo aplicativos de edição de áudio, vídeo, imagem e outros mais especializados. Infelizmente, muitos dos programas mais famosos para tais funções são proprietários, onde a prática de altos custos, limitação do direito do que é produzido e a obsolência programada impactam negativamente o acesso e a experiência do usuário.


### Software livre

O software livre é todo aquele que possui uma licença permissiva sobre ser estudado, usado, modificado e distribuído e se mostra como uma experiência alternativa a essa realidade, já que os mesmos crescem através da colaboração coletiva, sem necessariamente ser gratuito, mas tendo seu código fonte sempre disponível e transparente, o que ajuda na segurança e no rápido solucionamento de bugs. Além disso, o software livre não está sujeito à mudanças abusivas de termos e licença e sua natureza acessível ajuda a reduzir custos.

![coruja e sapo / pixel art frog and owl](./.gitlab/coruja_sapo.webp)

### Objetivos

Com isso em mente, o objetivo deste trabalho é explorar os programas gratuitos de código livre que têm o potencial de serem usados na criação de um jogo, produzir um protótipo funcional utilizando-os e documentar tanto a experiência como a viabilidade de criar um jogo deste modo. O jogo se encontra em processo de desenvolvimento, seu código estará disponível em repositório público sob a licença GPLv3, e demais sob a licença Creative Commons, e se trata de um RPG 2D em pixel art, representando o IFBA Campus Jacobina, contendo desafios e quebra-cabeças divertidos referenciando a vivência dos estudantes. As ferramentas que mais estão sendo utilizados são: **[Godot](https://godotengine.org)** para motor de jogo; **[Krita](https://krita.org)** para criação de desenhos, sprites e animações; **[Musescore](https://musescore.org)** para composição de músicas; **[LMMS](https://lmms.io)** para mixagem de músicas; **[Git](https://git-scm.com)** para controle de versionamento; **GNU/Linux** como sistema operacional; **[LogSeq](https://logseq.com)** para anotações, organização e planejamento. Além dos softwares listados, vários outros têm o potencial de serem usados futuramente.

## Como jogar?

Baixe um **[executável para linux ou windows](https://gitlab.com/ifeim/Ifeim/-/releases)**. Caso queira aprender sobre como o jogo foi feito, baixe a versão 4.4 ou mais recente da **[Godot](https://godotengine.org)** e o projeto usando o comando `git clone https://gitlab.com/MaiaAf/Ifeim.git` no diretório desejado. Também é possível baixar o projeto como arquivo zip e arrasta-lo para dentro da Godot.

O jogo na versão web pelo Gitlab Pages ainda não funciona.

![ Desafio sobre o conceito de Exclusão Mútua / challange about the concept of Mutual Exclusion](./.gitlab/carrinhos.webp)
 
