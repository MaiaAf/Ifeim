extends AnimationPlayer
signal metade
@export var duracao_segundos : float = 1.0
@onready var timer: Timer = $Timer

func transicionar(duracao : float = duracao_segundos) -> void:
	timer.wait_time = duracao / 2
	timer.start()
	
	speed_scale = 1 / duracao
	Global.esconder_ui.emit(0, self.animation_finished)
	play("transicao")


func _on_timer_timeout() -> void:
	metade.emit()
