extends Node2D

@onready var andar_1: Node2D = $Anexo/Andar1

var andar : int = 0:
	set(n_andar):
		
		andar = n_andar
		var tween : Tween = get_tree().create_tween().set_trans(Tween.TRANS_CUBIC).set_ease(Tween.EASE_IN_OUT)
		tween.tween_property(andar_1, "modulate", Color(1,1,1,andar), 1)

func _ready() -> void:
	andar_1.modulate = Color(1,1,1, andar)

func _on_escada_body_entered(body: Node2D) -> void:
	if body is not Personagem: return
	andar = 0
	#print("opi")
	#andar_1.visible = not andar_1.visible
	#var tween : Tween = get_tree().create_tween().set_trans(Tween.TRANS_CUBIC).set_ease(Tween.EASE_IN_OUT)
	#tween.tween_property(andar_1, "modulate", Color(1,1,1,1 if andar_1.visible else 0), 1)
	#await tween.finished


func _on_escada_2_body_entered(body: Node2D) -> void:
	if body is not Personagem: return
	andar = 1
