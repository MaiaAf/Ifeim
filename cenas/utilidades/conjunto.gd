extends Area2D

var tween : Tween

@export var quantidade : float
@export var progresso : float:
	set(novo_progresso):
		progresso = novo_progresso
		atualizar_progresso.call_deferred()
@onready var shader : Sprite2D = $shader

func _ready() -> void:
	tween = get_tree().create_tween().set_trans(Tween.TRANS_CUBIC).set_ease(Tween.EASE_IN_OUT)
	tween.tween_method(set_shader_value, 1.0, 0, 3.0);
	await tween.finished
	progresso = 0.05

func atualizar_progresso() -> void:
	tween = get_tree().create_tween().set_trans(Tween.TRANS_CUBIC).set_ease(Tween.EASE_IN_OUT)
	var valor_atual : float = shader.material.get_shader_parameter("progresso")
	var valor_final : float
	# progresso if progresso == 0 else 
	valor_final = progresso / quantidade
	tween.tween_method(set_shader_value, valor_atual, valor_final, 2.0);


func set_shader_value(value: float) -> void:
	shader.material.set_shader_parameter("progresso", value)
