extends Node2D



@onready var desaparecer: Area2D = $Portaria/desaparecer


func _on_desaparecer_body_entered(body: Node2D) -> void:
	if body is not Personagem: return
	print("ahooo")
	var tween : Tween = get_tree().create_tween().set_trans(Tween.TRANS_CUBIC).set_ease(Tween.EASE_IN_OUT)
	tween.tween_property(desaparecer, "modulate", Color(1,1,1,0), 1)


func _on_desaparecer_body_exited(body: Node2D) -> void:
	if body is not Personagem: return
	var tween : Tween = get_tree().create_tween().set_trans(Tween.TRANS_CUBIC).set_ease(Tween.EASE_IN_OUT)
	tween.tween_property(desaparecer, "modulate", Color(1,1,1,1), 1)
