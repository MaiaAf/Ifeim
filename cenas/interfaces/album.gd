extends Control

const GATINHO_PORTARIA = preload("res://arte/fotos/gatinho-portaria.webp")
const ALBUM_FOTO = preload("res://componentes/album_foto.tscn")
const LAG = preload("res://arte/fotos/lag.webp")
var sessoes : Array[Array]

@onready var margin_container: MarginContainer = $MarginContainer
@onready var scroll_container: ScrollContainer = %ScrollContainer
@onready var container: VBoxContainer = %Container

func _ready() -> void:
	GerenciadorDeUi.definir_ancoras(self, 0.08, 0.08)
	var animais : Array = [
		"animaizinhos",
		["gatinho-portaria.webp",
		"gatinho-portaria.webp",
		"gatinho-portaria.webp",
		
		"gatinho-portaria.webp",
		"gatinho-portaria.webp",
		"gatinho-portaria.webp",
		]
	]
	var locais : Array = [
		"locais",
		["lag.webp",
		"lag.webp",
		"lag.webp",
		"lag.webp",
		]
	]
	
	sessoes.append(animais)
	sessoes.append(locais)

	
	gerar_album()

func gerar_album() -> void:
	print(sessoes)
	for sessao : Array in sessoes:
		var grid : GridContainer = GridContainer.new()
		var label : Label = Label.new()
		var separador : HSeparator = HSeparator.new()
		
		#for i in [container, label]:
			#i.set_h_size_flags(3)
			#i.set_v_size_flags(3)
		
		grid.set_columns(3)
		label.text = sessao[0]
		container.add_child(label)
		container.add_child(grid)
		container.add_child(separador)
		
		for foto : String in sessao[1]:
			var alb_foto : Control = ALBUM_FOTO.instantiate()
			alb_foto.foto = load("res://arte/fotos/" + foto)
			grid.add_child(alb_foto)


func _on_voltar_pressed() -> void:
	queue_free()
