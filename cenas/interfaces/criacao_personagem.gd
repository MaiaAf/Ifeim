extends PanelContainer

signal confirmar
var PlayerInfo : Dictionary

@onready var animacao: Node2D = %animacao

@onready var nome: LineEdit = $MarginContainer/HBoxContainer/ScrollContainer/MarginContainer/VBoxContainer/nome

func _ready() -> void:
	GerenciadorDeUi.definir_ancoras(self, 0.05)
	animacao.mudar_animação("andando")


func _on_cabelo_pressed() -> void:
	animacao.penteado += 1
	atualizar_anim()


func _on_cor_pressed() -> void:
	animacao.cor += 1
	atualizar_anim()


func _on_roupa_pressed() -> void:
	animacao.camisa += 1
	atualizar_anim()


func _on_cabelo_cor_pressed() -> void:
	animacao.cor_cabelo += 1
	atualizar_anim()

func atualizar_anim() -> void:
	animacao.configurar_anim()
	animacao.definir_cores()

func _on_cor_olho_pressed() -> void:
	animacao.cor_olho += 1
	atualizar_anim()


func _on_confirmar_pressed() -> void:
	SalvarJogo.jogo_salvo.connect(confirmar_escolha)
	SalvarJogo.save["PlayerInfo"] = {
		"nome" = nome.text,
		"cor" = animacao.cor,
		"cor_cabelo" = animacao.cor_cabelo,
		"cor_olho" = animacao.cor_olho,
		"penteado" = animacao.penteado,
		"camisa" = animacao.camisa,
	}
	SalvarJogo.salvar_jogo()


func confirmar_escolha() -> void:
	confirmar.emit()


func _on_voltar_pressed() -> void:
	hide()
